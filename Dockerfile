FROM python:3.8-slim
LABEL MAINTAINER="Gabriel Andrade"
    
ENV TZ=America/Fortaleza
RUN cp /usr/share/zoneinfo/America/Fortaleza /etc/localtime
RUN echo "America/Fortaleza" > /etc/timezone

RUN apt-get update && apt-get install -y build-essential default-libmysqlclient-dev

WORKDIR /app

COPY ./app /app/
COPY requirements/ /requirements
RUN pip install -r /requirements/dev.txt

ENTRYPOINT ["gunicorn", "--bind", ":8000", "core.wsgi.dev"]
EXPOSE 8000