ZONES = (
    ('us-east-1', "Norte da Virgínia"),
)

INSTANCE_TYPES = (
    ('t2.micro', "Free Tier"),
)

FRAMEWORKS = (
    ('Python', (
        ('ami-034c6624d5fcd556b', 'Django'),
    )),
)

ENGINES = (
    ('mysql', "MySQL"),
)

DB_INSTANCE_TYPES = (
    ('db.t2.micro', "db.t2.micro (1 vCPU + 1Gb RAM)"),
)