from django.contrib import admin

from .models import AWSAccount, Forms
from django.utils.safestring import mark_safe
from .forms import FormsAddForm, FormsEditForm

@admin.register(AWSAccount)
class AWSAccountAdmin(admin.ModelAdmin):
    model = AWSAccount

@admin.register(Forms)
class FormsAdmin(admin.ModelAdmin):
    model = Forms
    list_display = ['creator', 'client', 'project_name', 'update_info']

    @mark_safe
    def update_info(self, obj):
        base_url = 'update_info'
        forms_id = obj.id   # Desenvolver um hash de rds para passar como parâmetro oculto e secreto.
        user_id = obj.creator.id  # Desenvolver um hash de usuário para passar como parâmetro oculto e secreto.

        return f'<a target="" href="/{base_url}/{user_id}/{forms_id}"> Atualizar Informações </a>'
    update_info.short_description  = 'Ação'
    update_info.allow_tags = True

    def get_fieldsets(self, request, obj=None):
        if obj:
            return [
                    ("Informações do projeto", {'fields': [
                        'creator',
                        'client', 
                        'project_name'
                    ]}),
                    ("Tecnologias do projeto", {'fields': [
                        'framework', 
                        'db_engine', 
                        'instance_type', 
                        'zone',
                    ]}),
                    ("S3 homologação", {'fields': [
                        'hlg_s3_name', 
                        'hlg_s3_zone',
                    ]}),
                    ("S3 produção", {'fields': [
                        'prd_s3_name', 
                        'prd_s3_zone',
                    ]}),
                    ("RDS homologação", {'fields': [
                        'hlg_rds_name', 
                        'hlg_rds_zone',
                        'hlg_rds_host',
                        'hlg_rds_port',
                        'hlg_rds_status',
                        'hlg_rds_db_user',
                        'hlg_rds_db_password',
                        'hlg_rds_db_instance_type'
                    ]}),
                    ("RDS produção", {'fields': [
                        'prd_rds_name', 
                        'prd_rds_zone',
                        'prd_rds_host',
                        'prd_rds_port',
                        'prd_rds_status',
                        'prd_rds_db_user',
                        'prd_rds_db_password',
                        'prd_rds_db_instance_type'
                    ]}),
                    ("EC2", {'fields': [
                        'ec2_security_group_id', 
                        'ec2_security_group_name',
                    ]}),
                    ("EC2 homologação", {'fields': [
                        'hlg_ec2_instance_id', 
                        'hlg_ec2_instance_type',
                        'hlg_ec2_zone',
                        'hlg_ec2_public_dns',
                        'hlg_ec2_status',
                        'hlg_ec2_key_pair'
                    ]}),
                    ("EC2 produção", {'fields': [
                        'prd_ec2_instance_id', 
                        'prd_ec2_instance_type',
                        'prd_ec2_zone',
                        'prd_ec2_public_dns',
                        'prd_ec2_status',
                        'prd_ec2_key_pair'
                    ]}),
                ]

        return [
                    ("Informações do projeto", {'fields': [
                        'creator',
                        'client', 
                        'project_name'
                    ]}),
                    ("Tecnologias do projeto", {'fields': [
                        'framework', 
                        'db_engine', 
                        'instance_type', 
                        'zone',
                    ]})
                ]

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.form = FormsEditForm
        else:
            self.form = FormsAddForm
        return super(FormsAdmin, self).get_form(request, obj, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ('creator', 'client', 'project_name', 'framework', 'db_engine', 'instance_type', 'zone', 'hlg_s3_name', 'hlg_s3_zone', 'prd_s3_name', 'prd_s3_zone', 'hlg_rds_name', 'hlg_rds_zone', 'hlg_rds_host', 'hlg_rds_port', 'hlg_rds_status', 'hlg_rds_db_name', 'hlg_rds_db_user', 'hlg_rds_db_password', 'hlg_rds_db_instance_type', 'prd_rds_name', 'prd_rds_zone', 'prd_rds_host', 'prd_rds_port', 'prd_rds_status', 'prd_rds_db_name', 'prd_rds_db_user', 'prd_rds_db_password', 'prd_rds_db_instance_type', 'ec2_security_group_id', 'ec2_security_group_name', 'hlg_ec2_instance_id', 'hlg_ec2_instance_type', 'hlg_ec2_zone', 'hlg_ec2_public_dns', 'hlg_ec2_status', 'hlg_ec2_key_pair', 'prd_ec2_instance_id', 'prd_ec2_instance_type', 'prd_ec2_zone', 'prd_ec2_public_dns', 'prd_ec2_status', 'prd_ec2_key_pair')
        else:
            return self.readonly_fields
