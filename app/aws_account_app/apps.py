from django.apps import AppConfig


class AwsAccountAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aws_account_app'
