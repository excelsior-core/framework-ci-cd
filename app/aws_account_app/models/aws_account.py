from django.db import models

# Create your models here.
class AWSAccount(models.Model):
    nickname = models.CharField("Apelido", max_length=200)
    access_key = models.CharField("Chave de acesso", max_length=200)
    secret_key = models.CharField("Chave secreta", max_length=200)
    key_pair = models.TextField("Conteúdo da chave (.pub)")

    def __str__(self):
        return self.nickname

    class Meta:        
        verbose_name = "AWS Account"
        verbose_name_plural = "AWS Accounts"