import boto3
import time

from django.db import models
from .aws_account import AWSAccount
from aws_account_app.choices import ZONES, INSTANCE_TYPES, FRAMEWORKS, ENGINES, DB_INSTANCE_TYPES
from django.db.models.signals import post_delete, post_save, pre_save, pre_delete
from django.dispatch import receiver
from aws_account_app.utils import get_random_string


class Forms(models.Model):
    creator = models.ForeignKey(AWSAccount, verbose_name = "Criador", on_delete=models.CASCADE)
    client = models.CharField("Cliente", max_length=200)
    project_name = models.CharField("Nome do Projeto", max_length=200)
    framework = models.CharField("Framework", max_length=200, choices=FRAMEWORKS)
    db_engine = models.CharField("Banco de Dados", max_length=200, choices=ENGINES)
    instance_type = models.CharField("Tipo de Instância", max_length=200, choices=INSTANCE_TYPES)
    zone = models.CharField("Zona", max_length=200, choices=ZONES)

    # Values that comes from AWS
    # S3 - HLG
    hlg_s3_name = models.CharField("Nome do Bucket", max_length=200)
    hlg_s3_zone = models.CharField("Zona do Bucket", max_length=200)

    # S3 - PRD
    prd_s3_name = models.CharField("Nome do Bucket", max_length=200)
    prd_s3_zone = models.CharField("Zona do Bucket", max_length=200)

    # RDS - HLG
    hlg_rds_name = models.CharField("Identificador da instância", max_length=200)
    hlg_rds_zone = models.CharField("Zona", max_length=200)
    hlg_rds_host = models.CharField("Host", max_length=200)
    hlg_rds_port = models.CharField("Porta", max_length=200)
    hlg_rds_status = models.CharField("Status", max_length=200)
    hlg_rds_db_user = models.CharField("Usuário do Banco de dados", max_length=200)
    hlg_rds_db_password = models.CharField("Senha do Banco de dados", max_length=200) #Deve ser readonly
    hlg_rds_db_instance_type = models.CharField("Tipo de Instância", max_length=200)

    # RDS - PRD
    prd_rds_name = models.CharField("Identificador da instância", max_length=200)
    prd_rds_zone = models.CharField("Zona", max_length=200)
    prd_rds_host = models.CharField("Host", max_length=200)
    prd_rds_port = models.CharField("Porta", max_length=200)
    prd_rds_status = models.CharField("Status", max_length=200)
    prd_rds_db_user = models.CharField("Usuário do Banco de dados", max_length=200)
    prd_rds_db_password = models.CharField("Senha do Banco de dados", max_length=200) #Deve ser readonly
    prd_rds_db_instance_type = models.CharField("Tipo de Instância", max_length=200)

    # EC2 - Common
    ec2_security_group_id = models.CharField("ID do Grupo de Segurança", max_length=200, null=True, blank=True)
    ec2_security_group_name = models.CharField("Nome do Grupo de Segurança", max_length=200, null=True, blank=True)

    # EC2 - HLG
    hlg_ec2_instance_id = models.CharField("ID da Instância", max_length=200, null=True, blank=True)
    hlg_ec2_instance_type = models.CharField("Tipo de Instância", max_length=200)
    hlg_ec2_zone = models.CharField("Zona", max_length=200)
    hlg_ec2_public_dns = models.CharField("DNS público", max_length=200)
    hlg_ec2_status = models.CharField("Status da instância", max_length=200)
    hlg_ec2_key_pair = models.CharField("Nome da chave de acesso", max_length=200)

    # EC2 - PRD
    prd_ec2_instance_id = models.CharField("ID da Instância", max_length=200, null=True, blank=True)
    prd_ec2_instance_type = models.CharField("Tipo de Instância", max_length=200)
    prd_ec2_zone = models.CharField("Zona", max_length=200)
    prd_ec2_public_dns = models.CharField("DNS público", max_length=200)
    prd_ec2_status = models.CharField("Status da instância", max_length=200)
    prd_ec2_key_pair = models.CharField("Nome da chave de acesso", max_length=200)

    def clean(self):
        from django.forms import ValidationError
        message = {}
        hlg_bucket_name = self.hlg_s3_name
        prd_bucket_name = self.prd_s3_name

        s3 = boto3.resource('s3',
            aws_access_key_id=self.creator.access_key,
            aws_secret_access_key=self.creator.secret_key)
        hlg_bucket = s3.Bucket(hlg_bucket_name)
        prd_bucket = s3.Bucket(prd_bucket_name)
        if hlg_bucket.creation_date or prd_bucket.creation_date:
            message['hlg_bucket_name'] = "Este nome de bucket já existe, por gentileza, escolha outro!"
            message['prd_bucket_name'] = "Este nome de bucket já existe, por gentileza, escolha outro!"

        # Create more validations on these forms

        raise ValidationError(message)

    class Meta:        
        verbose_name = "Formulário de criação de esteiras"
        verbose_name_plural = "Formulários de criação de esteiras"

# Actions

@receiver(pre_save, sender=Forms)
def define_parameters(sender, instance, using, **kwargs):
    if Forms.objects.filter(id = instance.id).exists():
        return
    else:
        lower_project = (instance.project_name).lower()
        lower_client = (instance.client).lower()
        key_name = (instance.project_name).capitalize()
        
        instance.hlg_s3_name = f'{lower_project}-{lower_client}-hlg'
        instance.prd_s3_name = f'{lower_project}-{lower_client}-prd'
        instance.hlg_rds_name = f'{lower_project}-{lower_client}-hlg'
        instance.prd_rds_name = f'{lower_project}-{lower_client}-prd'
        instance.hlg_rds_db_instance_type = 'db.t2.micro'
        instance.prd_rds_db_instance_type = 'db.t2.micro'
        instance.hlg_s3_zone = instance.zone
        instance.prd_s3_zone = instance.zone
        instance.hlg_rds_zone = instance.zone
        instance.prd_rds_zone = instance.zone
        instance.hlg_ec2_zone = instance.zone
        instance.prd_ec2_zone = instance.zone
        instance.hlg_ec2_key_pair = f'{key_name}Key'
        instance.prd_ec2_key_pair = f'{key_name}Key'

        master_password = get_random_string(16)
        instance.hlg_rds_db_password = master_password
        master_password = get_random_string(16)
        instance.prd_rds_db_password = master_password

@receiver(post_save, sender=Forms)
def create_bucket(sender, instance, using, **kwargs):
    user = instance.creator
    hlg_bucket = instance.hlg_s3_name
    prd_bucket = instance.prd_s3_name

    s3_client = boto3.client('s3', 
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key)

    # S3 - HLG
    s3_client.create_bucket(Bucket=hlg_bucket)

    # S3 - PRD
    s3_client.create_bucket(Bucket=prd_bucket)

@receiver(pre_delete, sender=Forms)
def delete_bucket(sender, instance, using, **kwargs):
    user = instance.creator
    hlg_bucket = instance.hlg_s3_name
    prd_bucket = instance.prd_s3_name

    s3 = boto3.resource('s3', 
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key)
    
    # S3 - HLG
    hlg_bucket = s3.Bucket(hlg_bucket)
    
    # S3 - PRD
    prd_bucket = s3.Bucket(prd_bucket)

    hlg_bucket.delete()
    prd_bucket.delete()

@receiver(post_save, sender=Forms)
def create_rds(sender, instance, using, **kwargs):
    user = instance.creator

    rds_client = boto3.client('rds', 
            region_name=instance.zone,
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key)

    lower_project = (instance.project_name).lower()
    lower_client = (instance.client).lower()

    # RDS - HLG
    rds_client.create_db_instance(
        DBInstanceIdentifier=instance.hlg_rds_name,
        DBInstanceClass=instance.hlg_rds_db_instance_type,
        Engine=instance.db_engine,
        MasterUsername=f'oper_{lower_project}_{lower_client}',
        MasterUserPassword=instance.hlg_rds_db_password,
        BackupRetentionPeriod=7,
        MultiAZ=False,
        EngineVersion='5.7.34',
        AutoMinorVersionUpgrade=False,
        LicenseModel='general-public-license',
        PubliclyAccessible=True,
        StorageType='gp2',
        StorageEncrypted=False,
        EnableIAMDatabaseAuthentication=False,
        EnablePerformanceInsights=False,
        DeletionProtection=False,
        AllocatedStorage=30,
        MaxAllocatedStorage=1000,
        EnableCustomerOwnedIp=False,
    )

    # RDS - PRD
    rds_client.create_db_instance(
        DBInstanceIdentifier=instance.prd_rds_name,
        DBInstanceClass=instance.prd_rds_db_instance_type,
        Engine=instance.db_engine,
        MasterUsername=f'oper_{lower_project}_{lower_client}',
        MasterUserPassword=f'{instance.prd_rds_db_password}',
        BackupRetentionPeriod=7,
        MultiAZ=False,
        EngineVersion='5.7.34',
        AutoMinorVersionUpgrade=False,
        LicenseModel='general-public-license',
        PubliclyAccessible=True,
        StorageType='gp2',
        StorageEncrypted=False,
        EnableIAMDatabaseAuthentication=False,
        EnablePerformanceInsights=False,
        DeletionProtection=False,
        AllocatedStorage=30,
        MaxAllocatedStorage=1000,
        EnableCustomerOwnedIp=False,
    )

@receiver(pre_delete, sender=Forms)
def delete_rds(sender, instance, using, **kwargs):
    user = instance.creator
    hlg_rds_name = instance.hlg_rds_name
    prd_rds_name = instance.prd_rds_name

    rds_client = boto3.client('rds', 
            region_name=instance.zone,
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key
        )

    # RDS - HLG
    rds_client.delete_db_instance(
        DBInstanceIdentifier=hlg_rds_name,
        SkipFinalSnapshot=True,
        DeleteAutomatedBackups=True
    )

    # RDS - PRD
    rds_client.delete_db_instance(
        DBInstanceIdentifier=prd_rds_name,
        SkipFinalSnapshot=True,
        DeleteAutomatedBackups=True
    )

@receiver(post_save, sender=Forms)
def create_machine(sender, instance, using, **kwargs):
    user = instance.creator
    key_name = (instance.project_name).capitalize()

    ec2_client = boto3.client('ec2', 
            region_name=instance.zone,
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key)

    ec2_client.import_key_pair(
        DryRun=False,
        KeyName=f'{key_name}Key',
        PublicKeyMaterial=f'{user.key_pair}'.encode()
    )

    security_group = ec2_client.create_security_group(
        DryRun=False,
        Description='TCC default project example',
        GroupName=f'SG-TCC-{key_name}',
    )

    sg_groupid = security_group['GroupId']
    Forms.objects.filter(id = instance.id).update(
        ec2_security_group_id = sg_groupid, 
        ec2_security_group_name = f'SG-TCC-{key_name}')

    ec2_client.authorize_security_group_ingress(
        GroupId=sg_groupid,
        IpPermissions=[
            {
                'FromPort': 22,
                'IpProtocol': 'tcp',
                'IpRanges': [
                    {
                        'CidrIp': '0.0.0.0/0',
                        'Description': 'Free SSH access',
                    },
                ],
                'ToPort': 80,
            },
        ],
    )

    hlg_machine = ec2_client.run_instances(
        ImageId = instance.framework,  # Image ID
        InstanceType = instance.instance_type,
        KeyName = f'{key_name}Key',
        MaxCount = 1,
        MinCount = 1,
        Monitoring={
            'Enabled': False
        },
        SecurityGroupIds = [
            sg_groupid,
        ]
    )

    prd_machine = ec2_client.run_instances(
        ImageId = instance.framework,  # Image ID
        InstanceType = instance.instance_type,
        KeyName = f'{key_name}Key',
        MaxCount = 1,
        MinCount = 1,
        Monitoring={
            'Enabled': False
        },
        SecurityGroupIds = [
            sg_groupid,
        ]
    )
    
    Forms.objects.filter(id = instance.id).update(
        hlg_ec2_instance_id = hlg_machine['Instances'][0]['InstanceId'], 
        prd_ec2_instance_id = prd_machine['Instances'][0]['InstanceId']
    )

@receiver(pre_delete, sender=Forms)
def delete_machine(sender, instance, using, **kwargs):
    user = instance.creator
    key_name = (instance.project_name).capitalize()

    ec2_client = boto3.client('ec2', 
        region_name=instance.zone,
        aws_access_key_id= user.access_key, 
        aws_secret_access_key= user.secret_key)

    ec2_client.terminate_instances(
        InstanceIds=[
            instance.hlg_ec2_instance_id,
            instance.prd_ec2_instance_id,
        ],
        DryRun=False
    )

    time.sleep(10)

    ec2_client.delete_security_group(
        GroupId=instance.ec2_security_group_id,
        GroupName=instance.ec2_security_group_name,
        DryRun=False
    )

    ec2_client.delete_key_pair(
        KeyName=f'{key_name}Key',
    )