import boto3

from django.db import models
from .aws_account import AWSAccount
from django.dispatch import receiver
from aws_account_app.choices import ZONES, DB_INSTANCE_TYPES
from django.db.models.signals import post_delete, post_save, pre_save, pre_delete
from aws_account_app.utils import get_random_string


class RDS(models.Model):
    creator = models.ForeignKey(AWSAccount, on_delete=models.CASCADE)
    name = models.CharField("Identificador da instância", max_length=200)
    zone = models.CharField("Zona", max_length=200, choices=ZONES)
    host = models.CharField("Host", max_length=200)
    port = models.CharField("Porta", max_length=200)
    status = models.CharField("Status", max_length=200)
    db_name = models.CharField("Nome do Banco de dados", max_length=200, null=True, blank=True, 
                            help_text="Se nenhum nome for preenchido, uma base de dados default não será criada.")
    db_user = models.CharField("Usuário do Banco de dados", max_length=200)
    db_password = models.CharField("Senha do Banco de dados", max_length=200) #Deve ser readonly
    db_instance_type = models.CharField("Tipo de Instância", max_length=200, choices=DB_INSTANCE_TYPES)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "RDS"
        verbose_name_plural = "RDS's"

@receiver(pre_save, sender=RDS)
def define_db_password(sender, instance, using, **kwargs):
    if RDS.objects.filter(id = instance.id).exists():
        return
    else:
        master_password = get_random_string(16)
        instance.db_password = master_password

@receiver(post_save, sender=RDS)
def create_rds(sender, instance, using, **kwargs):
    user = instance.creator
    rds_name = instance.name

    rds_client = boto3.client('rds', 
            region_name=instance.zone,
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key)

    rds_client.create_db_instance(
        DBName=instance.db_name,
        DBInstanceIdentifier=rds_name,
        DBInstanceClass=instance.db_instance_type,
        Engine='mysql',
        MasterUsername=f'oper_{instance.db_name}',
        MasterUserPassword=f'{instance.db_password}',
        BackupRetentionPeriod=7,
        MultiAZ=False,
        EngineVersion='5.7.34',
        AutoMinorVersionUpgrade=False,
        LicenseModel='general-public-license',
        PubliclyAccessible=True,
        StorageType='gp2',
        StorageEncrypted=False,
        EnableIAMDatabaseAuthentication=False,
        EnablePerformanceInsights=False,
        DeletionProtection=False,
        AllocatedStorage=30,
        MaxAllocatedStorage=1000,
        EnableCustomerOwnedIp=False,
    )

@receiver(pre_delete, sender=RDS)
def delete_rds(sender, instance, using, **kwargs):
    user = instance.creator
    rds_name = instance.name
    
    rds_client = boto3.client('rds', 
            region_name=instance.zone,
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key
        )

    rds_client.delete_db_instance(
        DBInstanceIdentifier=rds_name,
        SkipFinalSnapshot=True,
        DeleteAutomatedBackups=True
    )