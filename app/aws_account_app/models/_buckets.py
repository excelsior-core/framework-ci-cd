import boto3

from django.db import models
from .aws_account import AWSAccount
from aws_account_app.choices import ZONES
from django.db.models.signals import post_delete, post_save, pre_save, pre_delete
from django.dispatch import receiver


class Buckets(models.Model):
    creator = models.ForeignKey(AWSAccount, on_delete=models.CASCADE)
    name = models.CharField("Nome do Bucket", max_length=200)
    zone = models.CharField("Zona", max_length=200, choices=ZONES)

    def clean(self):
        from django.forms import ValidationError
        message = {}
        bucket_name = self.name

        s3 = boto3.resource('s3',
            aws_access_key_id=self.creator.access_key,
            aws_secret_access_key=self.creator.secret_key)
        bucket = s3.Bucket(bucket_name)
        if bucket.creation_date:
            message['name'] = "Este nome de bucket já existe, por gentileza, escolha outro!"

        raise ValidationError(message)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Bucket"
        verbose_name_plural = "Buckets"

@receiver(post_save, sender=Buckets)
def create_bucket(sender, instance, using, **kwargs):
    user = instance.creator
    bucket = instance.name

    s3_client = boto3.client('s3', 
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key)
    s3_client.create_bucket(Bucket=bucket)

@receiver(pre_delete, sender=Buckets)
def delete_bucket(sender, instance, using, **kwargs):
    user = instance.creator
    bucket_name = instance.name

    s3 = boto3.resource('s3', 
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key)
    bucket = s3.Bucket(bucket_name)
    bucket.delete()