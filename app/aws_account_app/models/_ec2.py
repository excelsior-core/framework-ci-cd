import boto3

from django.db import models
from .aws_account import AWSAccount
from aws_account_app.choices import ZONES, INSTANCE_TYPES
from django.db.models.signals import post_delete, post_save, pre_save, pre_delete
from django.dispatch import receiver

class Machines(models.Model):
    creator = models.ForeignKey(AWSAccount, on_delete=models.CASCADE)
    instance_type = models.CharField("Tipo de Instância", max_length=200, choices=INSTANCE_TYPES)
    zone = models.CharField("Zona", max_length=200, choices=ZONES)

    class Meta:        
        verbose_name = "Machine"
        verbose_name_plural = "Machines"

@receiver(post_save, sender=Machines)
def create_machine(sender, instance, using, **kwargs):
    user = instance.creator

    ec2_client = boto3.client('ec2', 
            region_name=instance.zone,
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key)

    exists = ec2_client.describe_key_pairs(
            KeyNames =[
                f'{user.nickname}Key'
            ],
        )

    if exists['KeyPairs'][0] == None:
        ec2_client.import_key_pair(
            DryRun=False,
            KeyName=f'{user.nickname}Key',
            PublicKeyMaterial=f'{user.key_pair}'.encode()
        )

    security_group = ec2_client.create_security_group(
        DryRun=False,
        Description='TCC default project example',
        GroupName='SG-TCC',
    )

    sg_groupid = security_group['GroupId']

    ec2_client.authorize_security_group_ingress(
        GroupId=sg_groupid,  # PEGAR O GROUP ID CRIADO
        IpPermissions=[
            {
                'FromPort': 22,
                'IpProtocol': 'tcp',
                'IpRanges': [
                    {
                        'CidrIp': '0.0.0.0/0',
                        'Description': 'Free SSH access',
                    },
                ],
                'ToPort': 80,
            },
        ],
    )

    ec2_client.run_instances(
        ImageId = 'ami-034c6624d5fcd556b',
        InstanceType = instance.instance_type,
        KeyName = f'{user.nickname}Key',
        MaxCount = 1,
        MinCount = 1,
        Monitoring={
            'Enabled': False
        },
        SecurityGroupIds = [
            'sg-0f682608e3355a794',
            'sg-067aedaeff552904b',
        ]
    )