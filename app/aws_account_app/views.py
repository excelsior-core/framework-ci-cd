import boto3
import json

from .models import AWSAccount, Forms
from django.shortcuts import redirect, render
from django.urls import reverse
from django.http.response import HttpResponse, HttpResponseRedirect


def redirect_to_admin(request):
    return redirect("/admin")

def update_info(request, user_id, forms_id):
    forms = Forms.objects.get(id = forms_id)
    user = AWSAccount.objects.get(id = user_id)

    rds_client = boto3.client('rds', 
            region_name=forms.zone,
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key)

    hlg_rds_value = rds_client.describe_db_instances(
        DBInstanceIdentifier=forms.hlg_rds_name,
    )

    prd_rds_value = rds_client.describe_db_instances(
        DBInstanceIdentifier=forms.prd_rds_name,
    )

    Forms.objects.filter(id = forms_id).update(
        hlg_rds_host = hlg_rds_value['DBInstances'][0]['Endpoint']['Address'],
        hlg_rds_port = hlg_rds_value['DBInstances'][0]['Endpoint']['Port'],
        hlg_rds_db_user = hlg_rds_value['DBInstances'][0]['MasterUsername'],
        hlg_rds_status = hlg_rds_value['DBInstances'][0]['DBInstanceStatus'],

        prd_rds_host = prd_rds_value['DBInstances'][0]['Endpoint']['Address'],
        prd_rds_port = prd_rds_value['DBInstances'][0]['Endpoint']['Port'],
        prd_rds_db_user = prd_rds_value['DBInstances'][0]['MasterUsername'],
        prd_rds_status = prd_rds_value['DBInstances'][0]['DBInstanceStatus']
    )

    ec2_client = boto3.client('ec2',
            region_name=forms.zone,
            aws_access_key_id= user.access_key, 
            aws_secret_access_key= user.secret_key)

    hlg_ec2_value = ec2_client.describe_instances(
        InstanceIds=[
            forms.hlg_ec2_instance_id,
        ],
        DryRun=False
    )

    prd_ec2_value = ec2_client.describe_instances(
        InstanceIds=[
            forms.prd_ec2_instance_id,
        ],
        DryRun=False
    )

    Forms.objects.filter(id = forms_id).update(
        hlg_ec2_instance_type = hlg_ec2_value['Reservations'][0]['Instances'][0]['InstanceType'],
        hlg_ec2_public_dns = hlg_ec2_value['Reservations'][0]['Instances'][0]['PublicDnsName'],
        hlg_ec2_status = hlg_ec2_value['Reservations'][0]['Instances'][0]['State']['Name'],

        prd_ec2_instance_type = prd_ec2_value['Reservations'][0]['Instances'][0]['InstanceType'],
        prd_ec2_public_dns = prd_ec2_value['Reservations'][0]['Instances'][0]['PublicDnsName'],
        prd_ec2_status = prd_ec2_value['Reservations'][0]['Instances'][0]['State']['Name'],
    )

    return HttpResponseRedirect(reverse('admin:aws_account_app_forms_changelist'))
