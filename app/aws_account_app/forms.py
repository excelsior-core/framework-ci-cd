from django import forms
from .models import Forms

class FormsAddForm(forms.ModelForm):
    class Meta:
        model = Forms
        fields = ('creator', 'client', 'project_name', 'framework', 'db_engine', 'instance_type', 'zone')

class FormsEditForm(forms.ModelForm):
    class Meta:
        model = Forms
        fields = ('creator', 'client', 'project_name', 'framework', 'db_engine', 'instance_type', 'zone', 'hlg_s3_name', 'hlg_s3_zone', 'prd_s3_name', 'prd_s3_zone', 'hlg_rds_name', 'hlg_rds_zone', 'hlg_rds_host', 'hlg_rds_port', 'hlg_rds_status', 'hlg_rds_db_user', 'hlg_rds_db_password', 'hlg_rds_db_instance_type', 'prd_rds_name', 'prd_rds_zone', 'prd_rds_host', 'prd_rds_port', 'prd_rds_status', 'prd_rds_db_user', 'prd_rds_db_password', 'prd_rds_db_instance_type', 'ec2_security_group_id', 'ec2_security_group_name', 'hlg_ec2_instance_id', 'hlg_ec2_instance_type', 'hlg_ec2_zone', 'hlg_ec2_public_dns', 'hlg_ec2_status', 'hlg_ec2_key_pair', 'prd_ec2_instance_id', 'prd_ec2_instance_type', 'prd_ec2_zone', 'prd_ec2_public_dns', 'prd_ec2_status', 'prd_ec2_key_pair')